//---------------------------------------------------------------------------
#ifndef Assignment10H
#define Assignment10H
//---------------------------------------------------------------------------
#include "Experiment.h"
#include "LinearAlgebra.h"
#include "GLGeometryViewer3D.h"
//---------------------------------------------------------------------------

///
/// This is an example experiment.
///
/// The code is meant to demonstrate how
///  to use the GeoX framework
///
class Assignment10 : public Experiment
{
    GEOX_CLASS(Assignment10)

//Constructor / Destructor
public:
    Assignment10();
    virtual ~Assignment10();

//Methods
public:
    void addTriangles();
    void readFile();
    virtual QWidget *createViewer();

//Attributes
protected:
    GLGeometryViewer3D* viewer;
};


#endif                                         
