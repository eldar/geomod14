#ifndef ASSIGNMENT02_H
#define ASSIGNMENT02_H

#include "Experiment.h"
#include "LinearAlgebra.h"

#include <QScriptValue>

class GLGeometryViewer;
class QScriptEngine;

template <typename FloatType>
class DynamicVector;

class Assignment02 : public Experiment
{
    GEOX_CLASS(Assignment02)
public:
    Assignment02();
    virtual ~Assignment02();

    virtual QWidget* createViewer();

public:
    float evalFunc(float x);

    bool drawCurve();
    void drawTangentAndNormal();
    void interpolate();
    void approximate();

    Vector2f getPoint(int idx);

private:
    bool updateFunction();
    void drawPolynomial(const std::vector<double> &coeffs);

    GLGeometryViewer* viewer;
    QScriptEngine *scriptEngine;
    QScriptValue function;
    std::string funcString;
    std::vector<int> pointHandlers;

    float start, end;
    int32 numPoints;
    int32 tangentIndex;
    int32 approxDegree;
};

#endif // ASSIGNMENT02_H
