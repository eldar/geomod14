#include "stdafx.h"
#include "Assignment04.h"

#include "GLGeometryViewer.h"
#include "Properties.h"
#include "GeoXOutput.h"

#include <DynamicLinearAlgebra.h>

#include <QScriptEngine>
#include <QScriptProgram>
#include <QDebug>

IMPLEMENT_GEOX_CLASS( Assignment04, 0 )
{
   BEGIN_CLASS_INIT( Assignment04 );

   ADD_SEPARATOR("Numerical Values")
   ADD_BOOLEAN_PROP( display_intermediate, 0 );
   ADD_FLOAT32_PROP( display_t, 0 );
   ADD_FLOAT32_PROP( t_1, 0 );
   ADD_FLOAT32_PROP( t_2, 0 );
   ADD_FLOAT32_PROP( curve_thickness, 0);

   ADD_NOARGS_METHOD(Assignment04::drawCurve)
   ADD_NOARGS_METHOD(Assignment04::subdivide)
}

Assignment04::Assignment04()
{
    numControlPoints = 0;
    display_intermediate = false;
    display_t = 0.6;
    curve_thickness = 4.0;

    t_1 = 0.33;
    t_2 = 0.66;
}

Assignment04::~Assignment04()
{
}

QWidget* Assignment04::createViewer()
{
    viewer = new GLGeometryViewer();
    handler = new Assignment04_Handler(this);
    QObject::connect(viewer, SIGNAL(viewerContentChanged()),
                     handler, SLOT(onPointChanged()));
    return viewer;
}

void Assignment04::drawCurve()
{
    drawBezier(true);
}

static Vector4f getColor(float val, float min_val, float max_val)
{
    std::vector<Vector4f> colours;
    colours.push_back(makeVector4f(0, 0, 1, 1));
    colours.push_back(makeVector4f(1, 1, 1, 1));
    colours.push_back(makeVector4f(1, 0, 0, 1));

    float step = (max_val - min_val)/(colours.size()-1);
    for(int i = 0; i < colours.size() - 1; ++i)
    {
        float a = min_val + step*i;
        float b = a + step;
        if(val <= b)
        {
            auto x = (val - a)/step;
            return colours[i]*(1-x)+colours[i+1]*x;
        }
    }
    return colours[colours.size()-1];
}

static Line makeLine(int p1, int p2,
                     Vector4f color=makeVector4f(0,1,1,1),
                     float thickness = 1.0)
{
    Line line;
    line.vertices[0] = p1;
    line.vertices[1] = p2;
    line.color = color;
    line.thickness = thickness;
    return line;
}

void Assignment04::drawControlPoints(const std::vector<Vector2f> &ctrl_points, Vector4f color, bool can_be_modified)
{
    std::vector<int> indices;
    for(int i = 0; i < ctrl_points.size(); ++i)
    {
        Point2D pt;
        pt.position = ctrl_points[i];
        pt.canBeModified = can_be_modified;
        pt.color = makeVector4f(1.0, 1.0, 0.0, 1.0);
        indices.push_back(viewer->addPoint(pt));
    }
    for(int i = 0; i < ctrl_points.size()-1; ++i)
        viewer->addLine(makeLine(indices[i], indices[i+1], color));
}

void Assignment04::drawBezier(bool first)
{
    if(first)
    {
        numControlPoints = viewer->getNumberOfPoints();
        if(numControlPoints == 0)
            return;
    }

    // copy control points
    std::vector<Vector2f> control_points(numControlPoints);
    for(int i = 0; i < numControlPoints; ++i)
        control_points[i] = viewer->getPoint(i).position;

    viewer->clear();

    drawControlPoints(control_points, makeVector4f(0.0, 1.0, 1.0, 1.0), true);

    drawBezier(control_points, display_intermediate);

    viewer->refresh();
}

void Assignment04::drawBezier(const std::vector<Vector2f> &control_points, bool disp_interm, Vector4f colour)
{
    int numCtrlPts = control_points.size();
    int n = numCtrlPts-1; // degree of Bezier curve
    int numSegments = 100;

    std::vector<float> curvature;
    std::vector<Vector2f> points;
    std::vector<int> point_ids;

    int display_i = floor(display_t*numSegments);

    for(int i = 0; i <= numSegments; i++)
    {
        double t = double(i)/numSegments;

        // copy control points into array
        points = control_points;

        Vector2f cdot;
        Vector2f cdot2;
        for(int level = n; level > 0; level--)
        {
            // do linear interpolations
            for(int k=0; k<level; ++k)
                points[k] = points[k]*(1-t)+points[k+1]*t;
            // calculate derivatives
            if(level == 1)
                cdot=(points[1]-points[0])*n;
            else if(level == 2)
                cdot2=(points[2]-points[1]*2+points[0])*n*(n-1);
            // draw intermediate steps
            if(disp_interm && i == display_i)
            {
                int prev_id;
                for(int k=0; k<level; ++k)
                {
                    Point2D pt;
                    pt.position = points[k];
                    pt.size = 0;
                    pt.canBeModified = false;
                    int id = viewer->addPoint(pt);
                    if(k>0) {
                        viewer->addLine(makeLine(prev_id, id));
                    }
                    prev_id = id;
                }
            }
        }

        Point2D pt;
        pt.canBeModified = false;
        pt.size = 0;
        pt.position = points[0];

        float dot_mag = sqrt(cdot.getSqrNorm());
        float curv = (-cdot2[0]*cdot[1] + cdot2[1]*cdot[0])/pow(dot_mag, 3);

        curvature.push_back(curv);
        point_ids.push_back(viewer->addPoint(pt));
    }


    float min_curv = *std::min_element(curvature.begin(), curvature.end());
    float max_curv = *std::max_element(curvature.begin(), curvature.end());

    float max_mag = std::max(fabs(min_curv), fabs(max_curv));

    bool colourise = colour[3] == 0;
    // draw Bezier curve
    for(int i = 0; i < numSegments; ++i)
    {
        auto c = colourise ? getColor(curvature[i], -max_mag, max_mag) : colour;
        viewer->addLine(makeLine(point_ids[i], point_ids[i+1], c, curve_thickness));
    }
}

void Assignment04::subdivide()
{
    // copy control points
    std::vector<Vector2f> control_points(numControlPoints);
    for(int i = 0; i < numControlPoints; ++i)
        control_points[i] = viewer->getPoint(i).position;

    auto stuff = subdivide(control_points, t_2);
    auto stuff2 = subdivide(stuff.first, t_1/t_2);
    drawControlPoints(stuff2.first, makeVector4f(1,0,0,1), false);
    drawBezier(stuff2.first, false, makeVector4f(1,0,0,1));
    drawControlPoints(stuff2.second, makeVector4f(0,0,1,1), false);
    drawBezier(stuff2.second, false, makeVector4f(0,0,1,1));
    drawControlPoints(stuff.second, makeVector4f(0,1,0,1), false);
    drawBezier(stuff.second, false, makeVector4f(0,1,0,1));
    viewer->refresh();
}

std::pair<std::vector<Vector2f>, std::vector<Vector2f>> Assignment04::subdivide(const std::vector<Vector2f> &control_points, float t)
{
    std::pair<std::vector<Vector2f>, std::vector<Vector2f>> res;

    int numCtrlPts = control_points.size();
    int n = numCtrlPts-1; // degree of Bezier curve

    std::vector<Vector2f> points = control_points;

    res.first.push_back(points[0]);
    res.second.push_back(points[n]);
    for(int level = n; level > 0; level--)
    {
        // do linear interpolations
        for(int k=0; k<level; ++k)
            points[k] = points[k]*(1-t)+points[k+1]*t;
        res.first.push_back(points[0]);
        int idx_last = level == 0 ? 0 : level-1;
        res.second.push_back(points[idx_last]);
    }
    std::reverse(res.second.begin(), res.second.end());

    return res;
}

void Assignment04_Handler::onPointChanged()
{
    parent->drawBezier(false);
}
