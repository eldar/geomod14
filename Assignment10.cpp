//---------------------------------------------------------------------------
#include "stdafx.h"
//---------------------------------------------------------------------------
#include "Assignment10.h"
//---------------------------------------------------------------------------
#include "Properties.h"
#include "GLGeometryViewer3D.h"
#include "GeoXOutput.h"
//---------------------------------------------------------------------------

#include <fstream>
#include <QDebug>

IMPLEMENT_GEOX_CLASS( Assignment10 ,0)
{
    BEGIN_CLASS_INIT( Assignment10 );
    ADD_NOARGS_METHOD(Assignment10::addTriangles)
    ADD_NOARGS_METHOD(Assignment10::readFile)
}

QWidget *Assignment10::createViewer()
{
    viewer = new GLGeometryViewer3D();
    return viewer;
}

Assignment10::Assignment10()
{
    viewer = NULL;
}

Assignment10::~Assignment10() {}

void Assignment10::addTriangles()
{
    // first way to describe a point
    Point3D p1;
    p1.position  = makeVector3f(-3.0f,1.0f,1.0f);
    int p1Handle = viewer->addPoint(p1);

    // second way to describe a point
    Point3D p2(makeVector3f(-1.0f,1.0f,-2.0f));
    int p2Handle = viewer->addPoint(p2);

    // third way to describe a point
    Point3D p3(-1.0f,-1.0f,3.0f);
    int p3Handle = viewer->addPoint(p3);

    // then declare connectivity
    Triangle t1;
    t1.vertices[0] = p1Handle;
    t1.vertices[1] = p2Handle;
    t1.vertices[2] = p3Handle;
    viewer->addTriangle(t1);

    // or directly describe a triangle, be aware of introducing multiple new vertices
    viewer->addTriangle(makeVector3f(1.0,1.0,0.0),makeVector3f(3.0,1.0,3.0),makeVector3f(3.0,1.0,1.0));

    // display changes
    viewer->refresh();
}

namespace {
struct Trngl
{
    int v[3];
//    Trngl(int u, int v, int w)
//    {
//        v[0] = u;
//        v[1] = v;
//        v[2] = w;
//    }
};
}

void Assignment10::readFile()
{
    std::string fn = "/home/eldar/Dropbox/Uni/GeometricModelling/assignment10/bunny.smf";
    std::ifstream file(fn);
    std::string str;

    std::vector<Vector3f> vertices;
    std::vector<Trngl> faces;
    while (std::getline(file, str))
    {
        if(str[0]=='v')
        {
            float x,y,z;
            sscanf(str.c_str(),"v %f %f %f", &x, &y, &z);
            vertices.push_back(makeVector3f(x,y,z));
        }
        else if(str[0]=='f')
        {
            Trngl t;
            sscanf(str.c_str(),"f %d %d %d", t.v, t.v+1, t.v+2);
            faces.push_back(t);
        }
    }

    for(int i = 0; i < vertices.size(); ++i)
    {
        Point3D p;
        p.position  = vertices[i];
        viewer->addPoint(p);
    }
    for(int i = 0; i < faces.size(); ++i)
    {
        Triangle t1;
        Trngl t = faces[i];
        t1.vertices[0] = t.v[0]-1;
        t1.vertices[1] = t.v[1]-1;
        t1.vertices[2] = t.v[2]-1;
        viewer->addTriangle(t1);
    }
    viewer->refresh();
}

