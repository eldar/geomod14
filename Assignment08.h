#ifndef Assignment08_H
#define Assignment08_H

#include "Experiment.h"
#include "LinearAlgebra.h"
#include <VisTypes.h>

#include <QObject>

class GLGeometryViewer;

template <typename FloatType>
class DynamicVector;

class Assignment08 : public Experiment
{
    GEOX_CLASS(Assignment08)

public:
    Assignment08();
    virtual ~Assignment08();

    virtual QWidget* createViewer();

public:
    void drawCurve();
    void drawBezier(bool redraw);
    void drawBezier(const std::vector<Vector2f> &control_points, bool with_lines, Vector4f colour = makeVector4f(1,0,0,1));
    void drawBezierNonRational(const std::vector<Vector2f> &control_points, bool with_lines, Vector4f colour = makeVector4f(1,0,0,1));
    void drawFarin(const std::vector<Vector2f> &control_points);
    void drawEllipse();
    void tanks();

private:
    void drawControlPoints(const std::vector<Vector2f> &ctrl_points, Vector4f color, bool can_be_modified);

    GLGeometryViewer* viewer;

    int numControlPoints;

    std::vector<Point2D> control_points;
    float curve_thickness;

    float weight;
};

#endif // Assignment08_H
