#include "stdafx.h"
#include "Assignment02.h"

#include "GLGeometryViewer.h"
#include "Properties.h"
#include "GeoXOutput.h"

#include <DynamicLinearAlgebra.h>

#include <QScriptEngine>
#include <QScriptProgram>
#include <QDebug>

#include <eigen3/Eigen/Dense>

#include <iostream>

#include <lapacke.h>

IMPLEMENT_GEOX_CLASS( Assignment02, 0 )
{
   BEGIN_CLASS_INIT( Assignment02 );

   ADD_SEPARATOR("Numerical Values")
   ADD_FLOAT32_PROP( start, 0 )
   ADD_FLOAT32_PROP( end, 0 )
   ADD_INT32_PROP( numPoints, 0 )
   ADD_STRING_PROP(funcString, 0)
   ADD_INT32_PROP( tangentIndex, 0 )
   ADD_INT32_PROP( approxDegree, 0 )

   ADD_NOARGS_METHOD(Assignment02::drawCurve)
   ADD_NOARGS_METHOD(Assignment02::drawTangentAndNormal)
   ADD_NOARGS_METHOD(Assignment02::interpolate)
   ADD_NOARGS_METHOD(Assignment02::approximate)
}

Assignment02::Assignment02()
{
   scriptEngine = new QScriptEngine;
   numPoints = 11;
   funcString = "x*x";
   tangentIndex = 3;

   start = -2;
   end   = 2;

   approxDegree = 2;
}

Assignment02::~Assignment02()
{
}

QWidget* Assignment02::createViewer()
{
    viewer = new GLGeometryViewer();
    return viewer;
}

float Assignment02::evalFunc(float x)
{
    return function.call(function, QScriptValueList() << x).toNumber();
}

Line makeLine(int p1, int p2)
{
    Line line;
    line.vertices[0] = p1;
    line.vertices[1] = p2;
    return line;
}

bool Assignment02::drawCurve()
{
    if(!updateFunction())
        return false;
    float step = (end-start)/(numPoints-1);

    pointHandlers.clear();
    viewer->clear();

    int prev_handler = 0;
    for(int i = 0; i < numPoints; ++i)
    {
        float x = start + step * i;
        Vector2f curr = makeVector2f(x, evalFunc(x));
        Point2D pt(curr);
        pt.color = makeVector4f(1, 1, 0, 1);
        int handler = viewer->addPoint(pt);
        pointHandlers.push_back(handler);
        if(i != 0)
            viewer->addLine(makeLine(prev_handler, handler));
        prev_handler = handler;
    }

    viewer->refresh();
    return true;
}

void Assignment02::drawTangentAndNormal()
{
    int last = numPoints - 1;
    int i = tangentIndex;
    if(i < 0 || i > last)
        return;
    Vector2f curr = getPoint(i);
    Vector2f tan;
    if(i == 0)
        tan = getPoint(i+1)-curr;
    else if(i == last)
        tan = curr-getPoint(i-1);
    else
        tan = getPoint(i+1)-getPoint(i-1);
    tan.normalize();
    viewer->addLine(curr, curr+tan, makeVector4f(1, 0, 0, 1));
    Vector2f nrml = makeVector2f(tan[1], -tan[0]);
    viewer->addLine(curr, curr+nrml, makeVector4f(0, 1, 0, 1));
    viewer->refresh();
}

void Assignment02::interpolate()
{
    DynamicMatrix<double> B(numPoints, numPoints);
    DynamicVector<double> y(numPoints);
    for(int i = 0; i < numPoints; ++i)
    {
        Vector2f pt = getPoint(i);
        y[i] = pt[1];

        float x_pow = 1;
        // monomial bases
        for(int k = 0; k < numPoints; ++k)
        {
            B[k][i] = x_pow;
            x_pow *= pt[0];
        }
    }
    auto coeffs = invertMatrix(B)*y;
    output << coeffs << "\n";

//    drawPolynomial(coeffs);
    viewer->refresh();
}

static std::vector<double> toStd(const Eigen::VectorXd &vec)
{
    int size = vec.rows();
    std::vector<double> res(size);
    for(int i = 0; i < size; ++i)
        res[i] = vec(i);
    return res;
}

void Assignment02::approximate()
{
    int m = approxDegree+1;

    if(m > (numPoints))
    {
        output << "degree " << approxDegree << " is too large for the set of " << numPoints << " points\n";
        return;
    }

    Eigen::MatrixXd M(numPoints, m);
    Eigen::VectorXd y(numPoints);

    // lapack
    int mm = numPoints;
    int nn = m;
    int nrhs = 1;
    int lda = nn;
    int ldb = 1;

    std::vector<double> a(mm*nn);
    std::vector<double> b(mm);

    int it = 0;
    for(int i = 0; i < numPoints; ++i)
    {
        Vector2f pt = getPoint(i);
        y(i) = pt[1];
        b[i] = pt[1];

        double x_pow = 1;
        // monomial bases
        for(int k = 0; k < m; ++k)
        {
            M(i, k) = x_pow;
            a[it++]=x_pow;
            x_pow *= pt[0];
        }
    }

    std::cout << M << std::endl;
    std::cout << "crap" << std::endl;
    std::cout << y << std::endl;

    auto coeffs2 = M.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(y);

    auto M_t = M.transpose();
    auto My = M_t * y;

//    auto coeffs2 = (M_t * M).ldlt().solve(My);
    auto solution = (M_t * M).inverse() * My;
    cout << "solution" << endl;
    cout << solution << endl;
    cout << coeffs2 << endl;

    std::vector<int> jpvt(nn);
    std::fill(jpvt.begin(), jpvt.end(), 0);
    double rcond = 1E-8;
    int rank = 0;
//    lapack_int info = LAPACKE_dgels(LAPACK_ROW_MAJOR,'N',mm,nn,nrhs,a.data(),lda,b.data(),ldb);
    lapack_int info = LAPACKE_dgelsy(LAPACK_ROW_MAJOR, mm, nn,
                                     nrhs, a.data(), lda,
                                     b.data(), ldb, jpvt.data(),
                                     rcond, &rank);
    cout << "hello" << endl;
    for(int k = 0; k < nn; ++k)
        std::cout << "koef " << b[k] << endl;

    drawPolynomial(toStd(solution));
    viewer->refresh();
}

Vector2f Assignment02::getPoint(int idx)
{
    return viewer->getPoint(pointHandlers[idx]).position;
}

bool Assignment02::updateFunction()
{
    QScriptValue object = scriptEngine->evaluate(
                QString("({ foo: function(x) { return (%1); } })")
                .arg(QString::fromStdString(funcString)));
    if(scriptEngine->hasUncaughtException())
    {
        output << "Error occured compiling function\n";
        return false;
    }
    function = object.property("foo");
    return true;
}

void Assignment02::drawPolynomial(const std::vector<double> &coeffs)
{
    int intNumPoints = (numPoints-1)*20;
    float step = 2*(end-start)/intNumPoints;
    Vector2f prev;
    for(int i = 0; i <= intNumPoints; ++i)
    {
        double x = 2*start + step * i;
        double x_pow = 1;
        double sum = 0.0;
        // compute interpolation polynomial
        for(int k = 0; k < coeffs.size(); ++k)
        {
            sum += coeffs[k]*x_pow;
            x_pow *= x;
        }
        Vector2f curr = makeVector2f(x, float(sum));
        if(i != 0)
            viewer->addLine(prev, curr, makeVector4f(1, 0, 1, 1));
        prev = curr;
    }
}
