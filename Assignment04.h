#ifndef Assignment04_H
#define Assignment04_H

#include "Experiment.h"
#include "LinearAlgebra.h"
#include <VisTypes.h>

#include <QObject>

class GLGeometryViewer;

template <typename FloatType>
class DynamicVector;

class Assignment04;
class Assignment04_Handler : public QObject
{
    Q_OBJECT

public:
    Assignment04_Handler(Assignment04 *p) :
        parent(p)
    {
    }

public slots:
    void onPointChanged();

private:
    friend class Assignment04;
    Assignment04 *parent;
};

class Assignment04 : public Experiment
{
    GEOX_CLASS(Assignment04)

public:
    Assignment04();
    virtual ~Assignment04();

    virtual QWidget* createViewer();

public:
    void drawCurve();
    void drawBezier(bool redraw);
    void drawBezier(const std::vector<Vector2f> &control_points, bool disp_interm, Vector4f colour = makeVector4f(0,0,0,0));
    void subdivide();
    std::pair<std::vector<Vector2f>, std::vector<Vector2f>> subdivide(const std::vector<Vector2f> &control_points, float t);

private:
    void drawControlPoints(const std::vector<Vector2f> &ctrl_points, Vector4f color, bool can_be_modified);

    GLGeometryViewer* viewer;

    Assignment04_Handler *handler;
    int numControlPoints;

    std::vector<Point2D> control_points;
    float curve_thickness;

    bool display_intermediate;
    float display_t;
    float t_1, t_2;
};

#endif // Assignment04_H
