#include "stdafx.h"
#include "Assignment08.h"

#include "GLGeometryViewer.h"
#include "Properties.h"
#include "GeoXOutput.h"

#include <DynamicLinearAlgebra.h>

#include <QScriptEngine>
#include <QScriptProgram>
#include <QDebug>

#include <eigen3/Eigen/Dense>


IMPLEMENT_GEOX_CLASS( Assignment08, 0 )
{
   BEGIN_CLASS_INIT( Assignment08 );

   ADD_SEPARATOR("Numerical Values")
   ADD_FLOAT32_PROP( weight, 0 );
   ADD_FLOAT32_PROP( curve_thickness, 0);

   ADD_NOARGS_METHOD(Assignment08::drawCurve)
   ADD_NOARGS_METHOD(Assignment08::drawEllipse)
   ADD_NOARGS_METHOD(Assignment08::tanks)
}

Assignment08::Assignment08()
{
    numControlPoints = 0;
    curve_thickness = 4.0;

    weight = 0.5*sqrt(2);
}

Assignment08::~Assignment08()
{
}

QWidget* Assignment08::createViewer()
{
    viewer = new GLGeometryViewer();
    return viewer;
}

void Assignment08::drawCurve()
{
    drawBezier(true);
}

static Vector4f getColor(float val, float min_val, float max_val)
{
    std::vector<Vector4f> colours;
    colours.push_back(makeVector4f(0, 0, 1, 1));
    colours.push_back(makeVector4f(1, 1, 1, 1));
    colours.push_back(makeVector4f(1, 0, 0, 1));

    float step = (max_val - min_val)/(colours.size()-1);
    for(int i = 0; i < colours.size() - 1; ++i)
    {
        float a = min_val + step*i;
        float b = a + step;
        if(val <= b)
        {
            auto x = (val - a)/step;
            return colours[i]*(1-x)+colours[i+1]*x;
        }
    }
    return colours[colours.size()-1];
}

static Line makeLine(int p1, int p2,
                     Vector4f color=makeVector4f(0,1,1,1),
                     float thickness = 1.0)
{
    Line line;
    line.vertices[0] = p1;
    line.vertices[1] = p2;
    line.color = color;
    line.thickness = thickness;
    return line;
}

void Assignment08::drawControlPoints(const std::vector<Vector2f> &ctrl_points, Vector4f color, bool can_be_modified)
{
    std::vector<int> indices;
    for(int i = 0; i < ctrl_points.size(); ++i)
    {
        Point2D pt;
        pt.position = ctrl_points[i];
        pt.canBeModified = can_be_modified;
        pt.color = makeVector4f(1.0, 1.0, 0.0, 1.0);
        indices.push_back(viewer->addPoint(pt));
    }
    for(int i = 0; i < ctrl_points.size()-1; ++i)
        viewer->addLine(makeLine(indices[i], indices[i+1], color));
}

void Assignment08::drawBezier(bool first)
{
    if(first)
    {
        numControlPoints = 3;
        if(numControlPoints == 0)
            return;
    }

    // copy control points
    std::vector<Vector2f> control_points(numControlPoints);
    for(int i = 0; i < numControlPoints; ++i)
        control_points[i] = viewer->getPoint(i).position;

    drawControlPoints(control_points, makeVector4f(0.0, 1.0, 1.0, 1.0), true);

    drawBezier(control_points, false);

    viewer->refresh();
}

void Assignment08::drawBezier(const std::vector<Vector2f> &control_points, bool with_lines, Vector4f colour)
{
    int numCtrlPts = control_points.size();
    int n = numCtrlPts-1; // degree of Bezier curve
    int numSegments = 100;

    std::vector<Vector2f> points;
    std::vector<float> start_weights;
    start_weights.push_back(1);
    start_weights.push_back(weight);
    start_weights.push_back(1);
    std::vector<int> point_ids;

    for(int i = 0; i <= numSegments; i++)
    {
        double t = double(i)/numSegments;

        // copy control points into array
        points = control_points;
        auto weights = start_weights;

        for(int level = n; level > 0; level--)
        {
            for(int k=0; k<level; ++k)
            {
                auto num = points[k]*((1-t)*weights[k])+points[k+1]*(t*weights[k+1]);
                weights[k] = weights[k]*(1-t)+weights[k+1]*t;
                points[k] = num / weights[k];
            }
        }

        Point2D pt;
        pt.canBeModified = false;
        pt.size = with_lines ? 0 : 2;
        pt.position = points[0];
        pt.color = colour;
        point_ids.push_back(viewer->addPoint(pt));
    }

    // draw Bezier curve
    if(with_lines)
    for(int i = 0; i < numSegments; ++i)
        viewer->addLine(makeLine(point_ids[i], point_ids[i+1], colour, curve_thickness));
}

void Assignment08::drawBezierNonRational(const std::vector<Vector2f> &control_points, bool with_lines, Vector4f colour)
{
    int numCtrlPts = control_points.size();
    int n = numCtrlPts-1; // degree of Bezier curve
    int numSegments = 100;

    std::vector<Vector2f> points;
    std::vector<float> start_weights;
    start_weights.push_back(1);
    start_weights.push_back(1);
    start_weights.push_back(1);
    std::vector<int> point_ids;

    for(int i = 0; i <= numSegments; i++)
    {
        double t = double(i)/numSegments;

        // copy control points into array
        points = control_points;
        auto weights = start_weights;

        for(int level = n; level > 0; level--)
        {
            for(int k=0; k<level; ++k)
            {
                auto num = points[k]*((1-t)*weights[k])+points[k+1]*(t*weights[k+1]);
                weights[k] = weights[k]*(1-t)+weights[k+1]*t;
                points[k] = num / weights[k];
            }
        }

        Point2D pt;
        pt.canBeModified = false;
        pt.size = with_lines ? 0 : 2;
        pt.position = points[0];
        pt.color = colour;
        point_ids.push_back(viewer->addPoint(pt));
    }

    // draw Bezier curve
    if(with_lines)
    for(int i = 0; i < numSegments; ++i)
        viewer->addLine(makeLine(point_ids[i], point_ids[i+1], colour, curve_thickness));
}

void Assignment08::drawFarin(const std::vector<Vector2f> &p)
{
    auto q0 = (p[0]+p[1]*weight)/(1+weight);
    auto q1 = (p[1]*weight+p[2])/(1+weight);

    Point2D pt;
    pt.size = 4;
    pt.color = makeVector4f(1,0,1,1);
    pt.position = q0;
    viewer->addPoint(pt);
    pt.position = q1;
    viewer->addPoint(pt);
}

void Assignment08::drawEllipse()
{
    std::vector<Vector2f> cps;
    cps.push_back(makeVector2f(0,-1));
    cps.push_back(makeVector2f(2,-1));
    cps.push_back(makeVector2f(2,0));
    drawBezier(cps, true, makeVector4f(1,0,0,1));
    drawControlPoints(cps, makeVector4f(0.0, 1.0, 1.0, 1.0), false);
    drawFarin(cps);

    cps = std::vector<Vector2f>();
    cps.push_back(makeVector2f(2,0));
    cps.push_back(makeVector2f(2,1));
    cps.push_back(makeVector2f(0,1));
    drawBezier(cps, true, makeVector4f(1,0,0,1));
    drawControlPoints(cps, makeVector4f(0.0, 1.0, 1.0, 1.0), false);
    drawFarin(cps);

    cps = std::vector<Vector2f>();
    cps.push_back(makeVector2f(0,1));
    cps.push_back(makeVector2f(-2,1));
    cps.push_back(makeVector2f(-2,0));
    drawBezier(cps, true, makeVector4f(1,0,0,1));
    drawControlPoints(cps, makeVector4f(0.0, 1.0, 1.0, 1.0), false);
    drawFarin(cps);

    cps = std::vector<Vector2f>();
    cps.push_back(makeVector2f(-2,0));
    cps.push_back(makeVector2f(-2,-1));
    cps.push_back(makeVector2f(0,-1));
    drawBezier(cps, true, makeVector4f(1,0,0,1));
    drawControlPoints(cps, makeVector4f(0.0, 1.0, 1.0, 1.0), false);
    drawFarin(cps);

    viewer->refresh();
}

static float get_rand()
{
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

void Assignment08::tanks()
{
    viewer->clear();

    float x1 = get_rand();
    float y1 = get_rand();
    float x2 = get_rand();
    float y2 = get_rand();

//    x1 = 0.828907;
//    y1 = 0.274855;
//    x2 = 0.0877829;
//    y2 = 0.940508;

    // solving quadratic equation
    float v0 = 4;
    float g = 9.8;

    float a = -g*pow(x2-x1,2)/(2*v0*v0);
    float b = (x2-x1);
    float c = y1-y2+a;

    float D = b*b-4*a*c;
    if(D<0) {
        output << "Target is unreacheable";
        return;
    }

    float r1 = (-b+sqrt(D))/(2*a);
    float r2 = (-b-sqrt(D))/(2*a);

    float th[2] = {atan(r1), atan(r2)};

    for(int k = 0; k < 2; ++k)
    {
        float theta = th[k];
        if(x2<x1)
            theta += M_PI;

        auto p = [g,x1,y1,v0,theta](float t){
            return makeVector2f(x1+v0*cos(theta)*t,
                                y1+v0*sin(theta)*t-g*t*t/2);
        };
        auto d = [g,x1,y1,v0,theta](float t){
            return makeVector2f(v0*cos(theta),
                                v0*sin(theta)-g*t);
        };
        float t1 = 0.0;
        float t2 = (x2-x1)/(v0*cos(theta));

        auto p1 = p(t1);
        auto d1 = d(t1);
        auto p2 = p(t2);
        auto d2 = d(t2);

        Eigen::MatrixXf A(2, 2);
        Eigen::VectorXf B(2);

        A(0,0) = d1[0];
        A(0,1) = -d2[0];
        A(1,0) = d1[1];
        A(1,1) = -d2[1];
        B(0) = p2[0]-p1[0];
        B(1) = p2[1]-p1[1];

        Eigen::VectorXf AB = A.inverse() * B;

        auto p15 = p1+d1*AB(0);

        std::vector<Vector2f> cps;
        cps.push_back(p1);
        cps.push_back(p15);
        cps.push_back(p2);

        drawControlPoints(cps, makeVector4f(0.0, 1.0, 1.0, 1.0), false);
        drawBezierNonRational(cps, true, makeVector4f(0,1,0,1));
    }

    Point2D pt;
    pt.position = makeVector2f(x1, y1);
    pt.color = makeVector4f(0,0,1,1);
    pt.size = 5;
    viewer->addPoint(pt);
    pt.position = makeVector2f(x2, y2);
    pt.color = makeVector4f(1,0,0,1);
    viewer->addPoint(pt);

    viewer->refresh();
}
